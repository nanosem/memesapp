//
//  DataManager.swift
//  MemesApp
//
//  Created by a on 20.02.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PKHUD

class DataManager {
    

    static let instanse = DataManager()
    private init() {}
    
    private(set) var memes = [Meme]()
    private(set) var createdMemes = [Meme]()
    
    // MARK: - Methods
    func addWebMeme (_ meme: Meme) {
        memes.append(meme)
    }
    
    func addLocalMeme(_ meme: Meme) {
        if let image = meme.image {
            if let data = UIImagePNGRepresentation(image) {
                let filename = Utils.getDocumentsDirectory().appendingPathComponent("\(Utils.randomString()).png")
                try? data.write(to: filename)
                createdMemes.append(meme)
            }
        }
        NotificationCenter.default.post(name: .localMemeAdded, object: nil)
    }
    
    func deleteMeme(meme: Meme) {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileManager = FileManager.default
        do {
            var directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            var counter = 0
            for currentMeme in createdMemes {
                if currentMeme.id == meme.id {
                    createdMemes.remove(at: counter)
                    try fileManager.removeItem(at: directoryContents[counter])
                }
                counter += 1
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        NotificationCenter.default.post(name: .localMemeDeleted, object: nil)
    }
    
    // MARK: - Data update
    func loadTemplates() {
        memes.removeAll()
        Alamofire.request("https://api.imgflip.com/get_memes").responseJSON { response in
            if let value = response.data {
                let json = JSON(data: value)
                for memeInfo in json["data"]["memes"].array! {
                    let imageUrl = URL(string: String(describing: memeInfo["url"].rawValue))
                    let meme = Meme(imageURL: imageUrl!)
                    self.addWebMeme(meme)
                }
                NotificationCenter.default.post(name: .templateLoaded, object: nil)
            }
        }
    }
    
    func getLocalMemes() {
        DispatchQueue.global(qos: .utility).async { [weak self] in
            let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            do {
                let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
                for content in directoryContents {
                    let image = UIImage(contentsOfFile: content.path)
                    self?.createdMemes.append(Meme(image: image!))
                }
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .localMemesLoaded, object: nil)
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
}
