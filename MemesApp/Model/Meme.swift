//
//  Meme.swift
//  MemesApp
//
//  Created by a on 20.02.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit

struct Meme {
    private static var counter = 0
    
    // MARK: - Variables
    var imageURL : URL
    var image : UIImage?
    var id : String
    
    // MARK: - Inits
    init(imageURL: URL, image: UIImage? = nil) {
        self.imageURL = imageURL
        self.image = image
        self.id = String(Meme.counter)
        Meme.counter += 1
    }
    
    init(image: UIImage) {
        self.imageURL = URL(string: "google.com")!
        self.image = image
        self.id = String(Meme.counter)
        Meme.counter += 1
    }
}
