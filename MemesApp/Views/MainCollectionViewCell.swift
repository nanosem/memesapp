//
//  MainCollectionViewCell.swift
//  MemesApp
//
//  Created by a on 20.02.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class MainCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Private fields
    @IBOutlet private weak  var imageBox: UIImageView!
    static let nibName = UINib(nibName: "\(MainCollectionViewCell.self)", bundle: nil)
    static let identifier = "\(MainCollectionViewCell.self)"
    
    // MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Update methods
    func updateImageFromUrl (_ imageURL: URL) {
        Alamofire.request(imageURL).responseImage { response in
            if let downloadedImage = response.value {
                self.imageBox.image = downloadedImage
            }
        }
    }
    
    func updateImage (image: UIImage) {
        self.imageBox.image = image
    }
}
