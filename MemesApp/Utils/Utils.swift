//
//  Utils.swift
//  MemesApp
//
//  Created by a on 27.02.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit

struct Utils {
    
    // MARK: - Print text into image
    static func drawText(_ text: NSString, in image: UIImage, atCenter center: CGPoint) -> UIImage {
        
        let textColor = UIColor.white
        let textFont = UIFont.systemFont(ofSize: 14)
        let fontAttributes = [NSFontAttributeName: textFont]
        let textWidth = text.size(attributes: fontAttributes).width
        
        let scale = UIScreen.main.scale
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor
            ] as [String: Any]
        
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let point = CGPoint(x: center.x - textWidth / 2, y: center.y)
        
        let rect = CGRect(origin: point, size: image.size)
        
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

    // MARK: - Get document directory
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        
        return documentsDirectory
    }

    // MARK: - Generate random string
    static func randomString() -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< 20 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
}
