//
//  SegueIdentifiers.swift
//  MemesApp
//
//  Created by a on 24.02.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import Foundation

struct SegueIdentifiers {
    static let AdditionalToAdjunction = "AdditionalToAdjunction"
    static let MainToDelete = "MainToRemove"
}
