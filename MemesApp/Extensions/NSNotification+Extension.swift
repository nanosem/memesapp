//
//  NSNotification+Extension.swift
//  MemesApp
//
//  Created by a on 22.02.17.
//  Copyright © 2017 nanosem. All rights reserved.
//
import UIKit

extension NSNotification.Name {
    
    static let templateLoaded = NSNotification.Name("templateLoaded")
    static let localMemeAdded = NSNotification.Name("localMemeAdded")
    static let localMemeDeleted = NSNotification.Name("localMemeDeleted")
    static let localMemesLoaded = NSNotification.Name("localMemesLoaded")
}
