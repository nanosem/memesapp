//
//  MainCollectionViewController.swift
//  MemesApp
//
//  Created by a on 20.02.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PKHUD

class MainCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.register(MainCollectionViewCell.nibName, forCellWithReuseIdentifier: MainCollectionViewCell.identifier)
        NotificationCenter.default.addObserver(self, selector: #selector(self.localMemeAdded), name: .localMemeAdded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.localMemeDeleted), name: .localMemeDeleted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.localMemesLoaded), name: .localMemesLoaded, object: nil)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        collectionView!.collectionViewLayout = layout
        
        HUD.show(.progress)
        DataManager.instanse.getLocalMemes()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.instanse.createdMemes.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainCollectionViewCell.identifier, for: indexPath) as! MainCollectionViewCell
        let image = DataManager.instanse.createdMemes[indexPath.row].image
        
        cell.layer.borderWidth = 1
        cell.updateImage(image: image!)
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = (collectionView.bounds.width / 2) - 16
        return CGSize(width: cellSize, height: cellSize)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentMeme: Meme = DataManager.instanse.createdMemes[indexPath.row]
        performSegue(withIdentifier: SegueIdentifiers.MainToDelete, sender: currentMeme)
    }
    
    // MARK: - Segue methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.MainToDelete {
            let destination = segue.destination as! DeleteViewController
            destination.meme = (sender as! Meme)
        }
    }
    
    // MARK: - Observer methods
    func localMemeAdded() {
        self.collectionView?.reloadData()
    }
    
    func localMemeDeleted() {
        self.collectionView?.reloadData()
    }
    
    func localMemesLoaded() {
        HUD.hide()
        self.collectionView?.reloadData()
    }
}
