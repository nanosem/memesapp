//
//  AdditionalCollectionViewController.swift
//  MemesApp
//
//  Created by a on 21.02.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PKHUD

class AdditionalViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.register(MainCollectionViewCell.nibName, forCellWithReuseIdentifier: MainCollectionViewCell.identifier)
        self.collectionView?.allowsSelection = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.templateLoaded), name: .templateLoaded, object: nil)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        collectionView!.collectionViewLayout = layout
        
        HUD.show(.progress)
        DataManager.instanse.loadTemplates()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.instanse.memes.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainCollectionViewCell.identifier, for: indexPath) as! MainCollectionViewCell
        let imageUrl = DataManager.instanse.memes[indexPath.row].imageURL
        
        cell.layer.borderWidth = 1
        cell.updateImageFromUrl(imageUrl)
        
        return cell
    }
    
    // MARK: -
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentMeme: Meme = DataManager.instanse.memes[indexPath.row]
        performSegue(withIdentifier: SegueIdentifiers.AdditionalToAdjunction, sender: currentMeme)
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = (collectionView.bounds.width / 3) - 12
        
        return CGSize(width: cellSize, height: cellSize)
    }
    
    // MARK: - Segues methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.AdditionalToAdjunction{
            let destination = segue.destination as! AdjunctionViewController
            destination.meme = (sender as! Meme)
        }
    }
    
    // MARK: - Observer methods
    func templateLoaded() {
        HUD.hide()
        collectionView?.reloadData()
    }
}
