//
//  AdjunctionViewController.swift
//  MemesApp
//
//  Created by a on 24.02.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class AdjunctionViewController: UIViewController, UINavigationControllerDelegate, UITextFieldDelegate {

    
    // MARK: - Private fields
    @IBOutlet private weak var imageBox: UIImageView!
    @IBOutlet private weak var topTextField: UITextField!
    @IBOutlet private weak var bottomTextField: UITextField!
    @IBOutlet private weak var topTextBox: UILabel!
    @IBOutlet private weak var bottomTextBox: UILabel!
    
    var meme: Meme?
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bottomTextField.addTarget(self, action: #selector(bottomTextFieldDidChange(_:)), for: .editingChanged)
        self.topTextField.addTarget(self, action: #selector(topTextFieldDidChange(_:)), for: .editingChanged)
        
        self.topTextField.delegate = self
        self.bottomTextField.delegate = self
        
        self.topTextBox.font = UIFont(name: "Helvetica Bold", size: 18)
        self.topTextBox.textColor = UIColor.white
        
        self.bottomTextBox.font = UIFont(name: "Helvetica Bold", size: 18)
        self.bottomTextBox.textColor = UIColor.white
        
        if meme != nil {
            Alamofire.request((meme?.imageURL)!).responseImage { response in
                if let downloadedImage = response.value {
                    self.imageBox.image = downloadedImage
                }
            }
        }
    }
    // MARK: - Text field delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let numberOfCharacters = text.characters.count
        
        return numberOfCharacters < 25
    }
    
    func topTextFieldDidChange(_ textField: UITextField) {
        self.topTextBox.text = textField.text
    }
    
    func bottomTextFieldDidChange(_ textField: UITextField) {
        self.bottomTextBox.text = textField.text
    }

    
    // MARK: - Private actions
    @IBAction private func save(_ sender: Any) {
        
        let topText = self.topTextBox.text! as NSString
        let bottomText = self.bottomTextBox.text! as NSString
        let image = self.imageBox.image
        
        var currentImage = Utils.drawText(topText, in: image!, atCenter: CGPoint(x: (image?.size.width)! / 2, y: 0))
        currentImage = Utils.drawText(bottomText, in: currentImage, atCenter: CGPoint(x: (image?.size.width)! / 2, y: (image?.size.height)!-20))
        
        let currentMeme = Meme(imageURL: (meme?.imageURL)!, image: currentImage)
        DataManager.instanse.addLocalMeme(currentMeme)
        
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    }
