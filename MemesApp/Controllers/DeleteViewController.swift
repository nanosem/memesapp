//
//  EditViewController.swift
//  MemesApp
//
//  Created by a on 26.02.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit

class DeleteViewController: UIViewController {

    // MARK: - Private fields
    @IBOutlet private weak var imageBox: UIImageView!
    var meme: Meme?
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageBox.layer.borderWidth = 0.3
        
        if meme != nil {
            self.imageBox.image = meme?.image
        }
    }
    
    // MARK: -
    @IBAction func deleteButton(_ sender: Any) {
        DataManager.instanse.deleteMeme(meme: meme!)
        _ = navigationController?.popViewController(animated: true)
    }
}
